//
//  ViewController.swift
//  Comp6001-9989819-Assessment 1
//
//  Created by #MEMETEAM on 10/08/17.
//  Copyright © 2017 #MEMETEAM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //Declaring the Outlets and default values.
    
    @IBOutlet var sliders: [UISlider]!
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var rOutlet: UILabel!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var gOutlet: UILabel!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var bOutlet: UILabel!
    
    @IBOutlet var labelBackgroundCollection: [UILabel]!
    @IBOutlet var buttonBackgroundCollection: [UIButton]!
    
    var redSliderValue:Float = 255.0
    var greenSliderValue:Float = 255.0
    var blueSliderValue:Float = 255.0
    

    var alpha = 1
    
    //This is the code to assign the values from the colour value variables to the selectedColours variable.
    
    var selectedColours: UIColor {
        get {
            return UIColor(red: CGFloat(redSliderValue/255), green: CGFloat(greenSliderValue/255), blue: CGFloat(blueSliderValue/255), alpha: CGFloat(alpha))
        }
    }
    
    //This is where the colour variables get the sliders' values.
    
    @IBAction func redSliderMoved(_ sender: Any) {
        redSliderValue = redSlider.value.rounded()
        rOutlet.text = "R:\(redSliderValue)"
    }
    @IBAction func greenSliderMoved(_ sender: Any) {
        greenSliderValue = greenSlider.value.rounded()
        gOutlet.text = "G:\(greenSliderValue)"
    }
    @IBAction func blueSliderMoved(_ sender: Any) {
        blueSliderValue = blueSlider.value.rounded()
        bOutlet.text = "B:\(blueSliderValue)"
    }

    //This is where the Background colour is set to the value of selctedColours.
    
    @IBAction func setColourAction(_ sender: Any) {
        view.backgroundColor = selectedColours
    }
    
    //Reset button functions (pretty much sets everything back to defaults).
    
    @IBAction func resetColourAction(_ sender: Any) {
        redSlider.value = 255.0
        redSliderValue = 255.0
        
        greenSlider.value = 255.0
        greenSliderValue = 255.0
        
        blueSlider.value = 255.0
        blueSliderValue = 255.0
        
        view.backgroundColor = selectedColours
        
        rOutlet.text = "R:"
        gOutlet.text = "G:"
        bOutlet.text = "B:"
    }
    
    //These are just to tidy the edges of the labels' and buttons' background.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for label in labelBackgroundCollection {
           
            label.layer.masksToBounds = true
            label.layer.cornerRadius = 5
            
        }
        for button in buttonBackgroundCollection {
            
            button.layer.masksToBounds = true
            button.layer.cornerRadius = 5
            
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

