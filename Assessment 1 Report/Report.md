# Cody Ludwig 
## Assessment 1 Report

- Learning Outcome One:
Demonstrate the knowledge of Graphical User Interface
programming conventions and standards.
<br>

### Objective <br>
This assessment is part 1 of 3 and builds up to an application portfolio.
The idea is that you get a clear understanding of what the conventions
are in your chosen module. Some have been discussed in class, but there
are more.
<br>
<br>
# Formatting Content
- It is recommended to make a layout that fits the size of the screen of an IOS device. And users should be able to see and use content with out scrolling and in the centre of the view.

<br>

# Touch Controls 
- Touch gestures should be utilised to make the app easy to use and smooth to operate

<br>

# Hit Targets
- targets for touch controls should be atleast 44 points by 44 points to be accurately tapped by a finger, just to improve the useability 

<br>

# Text Size
- Text should typically be at least 11 points so that it is easy to view at a typical viewing distance

<br>

# Contrast
- Theres need to be a decent amount of contrast between text and the background in order to see the text well enough.

<br>

# Spacing
- Text overlapping is probably really difficult to read so it's a great idea to space text out enough to be able to actually read it.

<br>

# Organization
- Content and content modifiers should be in a logical place relative to eachother so it is easy to follow and use.

<br>

# Alignment
- Text, images and content need to be aligned to show users how information is related.

<br>

# Consistancy
- An app should be consistant and familliar to make it easier for new users to use the app.

<br>

### Sources
- https://developer.apple.com/design/tips/ - Apple's design tips
- https://developer.apple.com/ios/human-interface-guidelines/overview/design-principles/ - iOS design tips

### Peer Review
- Good as, really helped me understand how to design apps on ios device. With this info i have found that there is a good knowledge base to start developing in swift.- Liam Mason-Webb